import { RouteObject, useRoutes } from "react-router-dom";
import DashboardLayout from "./components/layout/DashboardLayout";

import Dashboard from "./pages/Dashboard";
import AuthPage from "./pages/AuthPage";
import CustomerForm from "./pages/CustomerForm";

const routes: RouteObject[] = [
    {
        path: "/",
        element: <DashboardLayout />,
        children: [
            {
                path: "",
                element: (
                    <Dashboard />
                ),
            },
            {
                path: "/customer/create",
                element: (
                    <CustomerForm />
                ),
            },
        ],
    },
    {
        path: "/auth/login",
        element: (
            <AuthPage />
        ),
    },
];

export default function Router() {
    return useRoutes(routes);
}
