import UserTable from "../components/UserTable";
import { auth } from "../lib/services/entry";

export default function Dashboard() {
    const name = auth.currentUser?.displayName ?? auth.currentUser?.email;
    return (
        <div>
            <div className="text-center">
                <h2 className="text-2xl font-semibold mb-2">Welcome</h2>
                <p className="text-lg text-gray-700">{`Hello, ${name}`}</p>
            </div>

            <UserTable />
        </div>
    );
}
