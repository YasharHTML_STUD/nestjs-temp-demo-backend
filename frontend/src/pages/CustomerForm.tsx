import React, { useState } from "react";
import { Customer } from "../lib/types/customer"; // Import the types from the file where they are defined
import { getIdToken } from "firebase/auth";
import { auth } from "../lib/services/entry";
import { Link } from "react-router-dom";

function CustomerForm() {
    const [customer, setCustomer] = useState<Customer>({
        name: "",
        individual: {
            dob: {
                day: 0,
                month: 0,
                year: 0,
            },
            first_name: "",
            last_name: "",
        },
        company: {
            tax_id: "",
        },
        billing: {
            address: {
                city: "",
                country: "",
                line1: "",
                line2: "",
                postal_code: "",
                state: "",
            },
        },
        password: "",
        email: "",
        phone_number: "",
        type: "individual",
    });

    const handleChange = (
        e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
    ) => {
        const { name, value } = e.target;
        setCustomer({
            ...customer,
            [name]: value,
        });
    };

    const handleIndividualChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setCustomer({
            ...customer,
            individual: {
                ...customer.individual,
                [name]: value,
            },
        });
    };

    const handleCompanyChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setCustomer({
            ...customer,
            company: {
                ...customer.company,
                [name]: value,
            },
        });
    };

    const handleAddressChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setCustomer({
            ...customer,
            billing: {
                address: {
                    ...customer.billing.address,
                    [name]: value,
                },
            },
        });
    };

    const handleDobChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { name, value } = e.target;
        setCustomer({
            ...customer,
            individual: {
                ...customer.individual,
                dob: {
                    ...customer.individual.dob,
                    [name]: parseInt(value),
                },
            },
        } as any);
    };

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        async function createCustomer() {
            const token = await getIdToken(auth.currentUser!);
            const headers = new Headers();

            headers.append("Authorization", "Bearer " + token);
            headers.append("Content-Type", "application/json");
            let customerNew;
            if (customer.type === "individual") {
                const { company, ...data } = customer;
                customerNew = data;
            } else {
                const { individual, ...data } = customer;
                customerNew = data;
            }

            return fetch(import.meta.env.VITE_API_URL + "/api/customer", {
                headers,
                method: "POST",
                body: JSON.stringify(customerNew),
            }).then((res) => res.json());
        }

        createCustomer().then(console.log);
    };

    return (
        <div className="container mx-auto">
            <Link to={"/"}>Back</Link>
            <h1 className="text-2xl font-semibold mb-4">Create Customer</h1>
            <form onSubmit={handleSubmit}>
                <div className="grid grid-cols-2 gap-4">
                    <div>
                        <label
                            htmlFor="name"
                            className="block text-sm font-medium text-gray-700"
                        >
                            Name
                        </label>
                        <input
                            type="text"
                            id="name"
                            name="name"
                            value={customer.name}
                            onChange={handleChange}
                            className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                        />
                    </div>
                    <div>
                        <label
                            htmlFor="type"
                            className="block text-sm font-medium text-gray-700"
                        >
                            Type
                        </label>
                        <select
                            id="type"
                            name="type"
                            value={customer.type}
                            onChange={handleChange}
                            className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                        >
                            <option value="individual">Individual</option>
                        </select>
                    </div>
                </div>
                {customer.type === "individual" && (
                    <div className="mt-4">
                        <label className="block text-sm font-bold text-gray-700">
                            Individual Information
                        </label>
                        <div className="grid grid-cols-2 gap-4 mt-2">
                            <div>
                                <label
                                    htmlFor="first_name"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    First Name
                                </label>
                                <input
                                    type="text"
                                    id="first_name"
                                    name="first_name"
                                    value={customer.individual.first_name}
                                    onChange={handleIndividualChange}
                                    className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                                />
                            </div>
                            <div>
                                <label
                                    htmlFor="last_name"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Last Name
                                </label>
                                <input
                                    type="text"
                                    id="last_name"
                                    name="last_name"
                                    value={customer.individual.last_name}
                                    onChange={handleIndividualChange}
                                    className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                                />
                            </div>
                        </div>
                        <div className="grid grid-cols-3 gap-x-4 w-full">
                            <div className="mt-4">
                                <label
                                    htmlFor="day"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Day of Birth
                                </label>
                                <input
                                    type="number"
                                    id="day"
                                    name="day"
                                    value={customer.individual.dob?.day}
                                    onChange={handleDobChange}
                                    className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                                />
                            </div>
                            <div className="mt-4">
                                <label
                                    htmlFor="month"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Month of Birth
                                </label>
                                <input
                                    type="number"
                                    id="month"
                                    name="month"
                                    value={customer.individual.dob?.month}
                                    onChange={handleDobChange}
                                    className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                                />
                            </div>
                            <div className="mt-4">
                                <label
                                    htmlFor="year"
                                    className="block text-sm font-medium text-gray-700"
                                >
                                    Year of Birth
                                </label>
                                <input
                                    type="number"
                                    id="year"
                                    name="year"
                                    value={customer.individual.dob?.year}
                                    onChange={handleDobChange}
                                    className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                                />
                            </div>
                        </div>
                    </div>
                )}
                {customer.type === "company" && (
                    <div className="mt-4">
                        <label
                            htmlFor="tax_id"
                            className="block text-sm font-medium text-gray-700"
                        >
                            Tax ID
                        </label>
                        <input
                            type="text"
                            id="tax_id"
                            name="tax_id"
                            value={customer.company.tax_id}
                            onChange={handleCompanyChange}
                            className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                        />
                    </div>
                )}
                <div className="mt-4">
                    <label
                        htmlFor="email"
                        className="block text-sm font-medium text-gray-700"
                    >
                        Email
                    </label>
                    <input
                        type="email"
                        id="email"
                        name="email"
                        value={customer.email}
                        onChange={handleChange}
                        className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                    />
                </div>
                <div className="mt-4">
                    <label
                        htmlFor="phone_number"
                        className="block text-sm font-medium text-gray-700"
                    >
                        Phone Number
                    </label>
                    <input
                        type="tel"
                        id="phone_number"
                        name="phone_number"
                        value={customer.phone_number}
                        onChange={handleChange}
                        className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                    />
                </div>
                <div className="mt-4">
                    <label
                        htmlFor="password"
                        className="block text-sm font-medium text-gray-700"
                    >
                        Password
                    </label>
                    <input
                        type="password"
                        id="password"
                        name="password"
                        value={customer.password}
                        onChange={handleChange}
                        className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                    />
                </div>
                <div className="mt-4">
                    <label
                        htmlFor="line1"
                        className="block text-sm font-medium text-gray-700"
                    >
                        Address Line 1
                    </label>
                    <input
                        type="text"
                        id="line1"
                        name="line1"
                        value={customer.billing.address.line1}
                        onChange={handleAddressChange}
                        className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                    />
                </div>
                <div className="mt-4">
                    <label
                        htmlFor="line2"
                        className="block text-sm font-medium text-gray-700"
                    >
                        Address Line 2
                    </label>
                    <input
                        type="text"
                        id="line2"
                        name="line2"
                        value={customer.billing.address.line2}
                        onChange={handleAddressChange}
                        className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                    />
                </div>
                <div className="grid grid-cols-2 gap-4 mt-4">
                    <div>
                        <label
                            htmlFor="city"
                            className="block text-sm font-medium text-gray-700"
                        >
                            City
                        </label>
                        <input
                            type="text"
                            id="city"
                            name="city"
                            value={customer.billing.address.city}
                            onChange={handleAddressChange}
                            className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                        />
                    </div>
                    <div>
                        <label
                            htmlFor="state"
                            className="block text-sm font-medium text-gray-700"
                        >
                            State
                        </label>
                        <input
                            type="text"
                            id="state"
                            name="state"
                            value={customer.billing.address.state}
                            onChange={handleAddressChange}
                            className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                        />
                    </div>
                </div>
                <div className="grid grid-cols-2 gap-4 mt-4">
                    <div>
                        <label
                            htmlFor="postal_code"
                            className="block text-sm font-medium text-gray-700"
                        >
                            Postal Code
                        </label>
                        <input
                            type="text"
                            id="postal_code"
                            name="postal_code"
                            value={customer.billing.address.postal_code}
                            onChange={handleAddressChange}
                            className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                        />
                    </div>
                    <div className="mt-4">
                        <label
                            htmlFor="country"
                            className="block text-sm font-medium text-gray-700"
                        >
                            Country
                        </label>
                        <select
                            id="country"
                            name="country"
                            value={customer.billing.address.country}
                            onChange={(e) =>
                                handleAddressChange({
                                    target: {
                                        name: e.target.name,
                                        value: e.target.value,
                                    },
                                } as any)
                            }
                            className="mt-1 p-2 border border-gray-300 rounded-md w-full"
                        >
                            <option value="">Select Country</option>
                            <option value="US">United States</option>
                            <option value="GB">United Kingdom</option>
                        </select>
                    </div>
                </div>
                <div className="mt-4">
                    <button
                        type="submit"
                        className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded-full"
                    >
                        Create Customer
                    </button>
                </div>
            </form>
        </div>
    );
}

export default CustomerForm;
