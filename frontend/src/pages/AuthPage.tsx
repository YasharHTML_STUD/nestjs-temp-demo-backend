import { useDispatch } from "react-redux";
import { signInWithGoogle } from "../lib/services/auth";
import { setAuthState } from "../lib/store/auth_slice";
import { AuthState } from "../lib/types/auth_state.enum";
import { useNavigate } from "react-router-dom";

export default function AuthPage() {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const handleSignIn = () =>
        signInWithGoogle()
            .then(() => {
                dispatch(setAuthState(AuthState.LOGGEDIN));
                navigate('/')
            })
            .catch((error) => {
                console.error(error);
                dispatch(setAuthState(AuthState.FAILED));
            });

    return (
        <div className="flex justify-center items-center h-screen bg-gray-100">
            <div className="text-center">
                <h1 className="text-4xl font-bold mb-8">Futuristic Banking</h1>
                <button
                    className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded-full"
                    onClick={handleSignIn}
                >
                    Sign in with Google
                </button>
            </div>
        </div>
    );
}
