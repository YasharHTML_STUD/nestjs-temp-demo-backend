import { useDispatch } from "react-redux";
import { auth } from "../../lib/services/entry";
import { setAuthState } from "../../lib/store/auth_slice";
import { AuthState } from "../../lib/types/auth_state.enum";

export default function Navbar() {
    const dispatch = useDispatch();
    const signOut = () =>
        auth
            .signOut()
            .then(() => dispatch(setAuthState(AuthState.LOGGEDOUT)))
            .catch((error) => {
                console.error(error);
                dispatch(setAuthState(AuthState.FAILED));
            });

    return (
        <nav className="bg-gray-800 p-4">
            <div className="max-w-7xl mx-auto flex justify-between items-center">
                <div className="flex items-center">
                    <span className="text-white text-lg font-semibold">
                        Demo
                    </span>
                </div>
                <div className="flex items-center space-x-4">
                    <button
                        className="bg-blue-500 hover:bg-blue-600 text-white font-bold py-2 px-4 rounded-full"
                        onClick={signOut}
                    >
                        Logout
                    </button>
                </div>
            </div>
        </nav>
    );
}
