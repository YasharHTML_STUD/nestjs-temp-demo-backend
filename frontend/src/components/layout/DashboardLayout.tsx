import { Outlet, useNavigate } from "react-router-dom";
import useAuth from "../../lib/hooks/useAuth";
import { AuthState } from "../../lib/types/auth_state.enum";
import Loading from "../Loading";
import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../../lib/services/entry";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { setAuthState } from "../../lib/store/auth_slice";
import Navbar from "./Navbar";

export default function DashboardLayout() {
    const authState = useAuth();
    const navigate = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            if (user) {
                dispatch(setAuthState(AuthState.LOGGEDIN));
            } else {
                dispatch(setAuthState(AuthState.LOGGEDOUT));
            }
        });
    }, []);

    if (authState === AuthState.LOADING) return <Loading />;
    if (authState !== AuthState.LOGGEDIN) navigate("/auth/login");

    return (
        <>
            <Navbar />
            <Outlet />
        </>
    );
}
