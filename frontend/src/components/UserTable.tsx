import { getIdToken } from 'firebase/auth';
import { auth } from '../lib/services/entry';
import { useEffect, useState } from 'react';
import Loading from './Loading';
import { useNavigate } from 'react-router-dom';

export default function UserTable() {
  const navigate = useNavigate();
  const [data, setData] = useState<any[]>([]);
  const [error, setError] = useState('');
  const [loaded, setLoaded] = useState(false);

  async function getCustomers() {
    const token = await getIdToken(auth.currentUser!);
    const headers = new Headers();
    headers.append('Authorization', 'Bearer ' + token);
    return fetch(import.meta.env.VITE_API_URL + '/api/customer', {
      headers,
    }).then((res) => res.json());
  }

  useEffect(() => {
    getCustomers()
      .then((data) => setData(data.data))
      .catch((error) => setError(error.message))
      .finally(() => setLoaded(true));
  }, []);

  if (!loaded) return <Loading />;
  if (error)
    return (
      <div
        className="bg-red-100 border border-red-400 text-red-700 px-4 py-3 rounded relative"
        role="alert"
      >
        <strong className="font-bold">Error!</strong>
        <span className="block sm:inline">{error}</span>
      </div>
    );

  return (
    <div className="container mx-auto">
      <div className="flex justify-between">
        <h1 className="text-2xl font-semibold mb-4">Customer List</h1>
        <button className='bg-blue-400 m-2 px-2 rounded text-white' onClick={() => navigate("/customer/create")}>Create Customer</button>
      </div>
      <div className="overflow-x-auto">
        <table className="table-auto w-full border-collapse border border-gray-200">
          <thead className="bg-gray-50">
            <tr>
              <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                ID
              </th>
              <th className="px-4 py-2">Fullname</th>
              <th className="px-4 py-2">Type</th>
              <th className="px-4 py-2">Email</th>
              <th className="px-4 py-2">Phone Number</th>
              <th className="px-4 py-2">Address</th>
              <th className="px-4 py-2">Date of Birth</th>
            </tr>
          </thead>

          <tbody className="bg-white divide-y divide-gray-200">
            {data.map((customer: any) => (
              <tr key={customer.id}>
                <td className="border px-4 py-2">
                  {customer.id}
                  <br />
                  {customer.metadata.uid}
                </td>
                <td className="border px-4 py-2">
                  {customer.individual.first_name}{' '}
                  {customer.individual.last_name}
                </td>
                <td className="border px-4 py-2">{customer.type}</td>
                <td className="border px-4 py-2">{customer.email}</td>
                <td className="border px-4 py-2">{customer.phone_number}</td>
                <td className="border px-4 py-2">
                  {customer.billing.address.line1},{' '}
                  {customer.billing.address.city},{' '}
                  {customer.billing.address.state},{' '}
                  {customer.billing.address.country},{' '}
                  {customer.billing.address.postal_code}
                </td>
                <td className="border px-4 py-2">
                  {customer.individual.dob.day}, {customer.individual.dob.month}
                  , {customer.individual.dob.year}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
