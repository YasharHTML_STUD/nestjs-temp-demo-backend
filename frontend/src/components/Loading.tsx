export default function Loading() {
    return (
        <div className="flex justify-center items-center h-screen bg-gray-800 text-white">
            <div className="flex space-x-4">
                <div className="w-12 h-12 bg-white rounded-full animate-bounce"></div>
                <div className="w-12 h-12 bg-white rounded-full animate-bounce"></div>
                <div className="w-12 h-12 bg-white rounded-full animate-bounce"></div>
            </div>
        </div>
    );
}
