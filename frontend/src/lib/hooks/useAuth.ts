import { useSelector } from "react-redux";
import { RootState } from "../store";
import { AuthState } from "../types/auth_state.enum";

export default function useAuth(): AuthState {
    const authState = useSelector(
        (state: RootState) => state.authReducer.authState
    );

    return authState;
}
