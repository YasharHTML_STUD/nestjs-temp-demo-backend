import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { auth } from "./entry";

export function signInWithGoogle() {
    const googleProvider = new GoogleAuthProvider();
    googleProvider.addScope("email");
    googleProvider.addScope("profile");
    return signInWithPopup(auth, googleProvider);
}
