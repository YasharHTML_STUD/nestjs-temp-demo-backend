import { PayloadAction, createSlice } from "@reduxjs/toolkit";
import { AuthState } from "../types/auth_state.enum";

const initialState: { authState: AuthState } = { authState: AuthState.LOADING };

const slice = createSlice({
    initialState,
    name: "AUTH_STATE",
    reducers: {
        setAuthState: (state, action: PayloadAction<AuthState>) => {
            state.authState = action.payload;
        },
    },
});

export const authReducer = slice.reducer;
export const { setAuthState } = slice.actions;