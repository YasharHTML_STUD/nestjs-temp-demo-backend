import { configureStore, combineReducers } from "@reduxjs/toolkit";
import { authReducer } from "./auth_slice";

const reducer = combineReducers({ authReducer });

export type RootState = ReturnType<typeof reducer>;

export const store = configureStore({
    reducer,
});
