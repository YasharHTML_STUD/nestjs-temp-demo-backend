export type Customer = {
    name: string;
    individual: Individual;
    company: Company;
    billing: Billing;
    password: string;
    email: string;
    phone_number: string;
    type: Type;
};

export type Dob = {
    day: number;
    month: number;
    year: number;
};

export type Individual = {
    dob?: Dob;
    first_name?: string;
    last_name?: string;
};

export type Company = {
    tax_id?: string;
};

export type Billing = {
    address: Address;
};

export type Address = {
    city: string;
    country: string;
    line1: string;
    line2?: string;
    postal_code: string;
    state?: string;
};

type Type = "company" | "individual";
