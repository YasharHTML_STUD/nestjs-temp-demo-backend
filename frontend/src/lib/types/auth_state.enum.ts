export enum AuthState {
    LOGGEDIN,
    LOGGEDOUT,
    FAILED,
    LOADING,
}
