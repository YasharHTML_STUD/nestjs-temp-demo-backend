import { Body, Controller, Get, Post, Query, UseGuards } from '@nestjs/common';
import { CustomerService } from './customer.service';
import { CustomerCreationDto } from '@app/types';
import Stripe from 'stripe';
import { AuthGuard } from '@app/security';

@Controller('customer')
export class CustomerController {
  constructor(private readonly customerService: CustomerService) {}

  @Get()
  @UseGuards(AuthGuard)
  getCustomers(@Query() params: Stripe.Issuing.CardholderListParams) {
    return this.customerService.getCustomers(params);
  }

  @Post()
  @UseGuards(AuthGuard)
  createCustomer(@Body() customerDto: CustomerCreationDto) {
    return this.customerService.createCustomer(customerDto);
  }
}
